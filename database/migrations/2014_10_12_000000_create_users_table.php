<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('firstname');
            $table->string('lastname');
            $table->string('position')->nullable();
            $table->string('phone')->nullable();
            $table->string('avatar')->default('noimage.png');
            $table->integer('companies_id')->nullable();
            $table->string('description')->nullable();
            $table->string('title')->nullable();
            $table->boolean('email_send')->default(0);
            $table->smallInteger('priority')->default(0);
            $table->boolean('visible')->default(1);
            $table->boolean('admin')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
