<?php

namespace App\Listeners;

use App\Events\UserPasswordSet;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class EmailNewPassword
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent  $event
     * @return void
     */
    public function handle(UserPasswordSet $event)
    {
        $user = $event->user;
        $data = [
            'firstname' => $user->firstname,
            'lastname'  => $user->lastname,
            'email'     => $user->email,
            'password'  => $event->plainPassword,
        ];

        Mail::send('emails.representative-info', $data, function($message) use ($user) {
            $message->to($user->email, $user->firstname." ".$user->lastname)->subject('SENS 2015 - Dane logowania');
        });
        $user->email_send = 1;
        $user->save();

        Log::info("Email with authorization data was sent to $user->email [$user->firstname, $user->lastname]");
    }
}
