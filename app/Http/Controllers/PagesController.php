<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Repositories\CompanyRepository;
use App\Repositories\UserRepository;
use Faker\Factory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class PagesController extends Controller {

    private $user;
    private $company;

    /**
     * PagesController constructor.
     * @param UserRepository $user
     * @param CompanyRepository $company
     */
    function __construct(UserRepository $user, CompanyRepository $company)
    {
        $this->user = $user;
        $this->company = $company;
    }

    public function getIndex(){

        $partners = $this->company->all();
       // $groups = $this->user->getUsersGroupedByCompany();
        return view('public.index', compact('partners', 'groups'));
    }

    public function getGallery(){
        $photos = File::files(public_path().'/assets/images/gallery');
        $result = array();
        foreach($photos as $photo){
            $image = Image::make($photo);
            $result[] = array($photo, $image->width(), $image->height());
        }
        return view('public.gallery')->with('images', $result);
    }

    public function getJurkowski(){
        return view('public.jurkowski');
    }

    public function getFoundation()
    {
        $faker = Factory::create('pl_PL');
        $team = Cache::remember('team', 10, function() use ($faker){
            $team = [];
            for($i=0; $i < 10; $i++){
                $team[$i]['avatarUrl'] = $faker->imageUrl(100,100,'people',true);
                $team[$i]['firstname'] = $faker->firstname;
                $team[$i]['lastname'] = $faker->lastname;
                $team[$i]['description'] = $faker->sentence;
                $team[$i] = (object)$team[$i];
            }

            return $team;
        });

        return view('public.'.App::getLocale().'.foundation')
            ->with('submenu', 'foundation')
            ->with(compact('team'));
    }

    public function getProjects()
    {
        return view('public.'.App::getLocale().'.projects')->with('submenu', 'projects');
    }


    public function getPartners()
    {
        $partners = Company::where('visible_front', 1)->get()->chunk(3);
        return view('public.'.App::getLocale().'.partners')
            ->with('partners', $partners);
    }

    public function getNewsroom()
    {
        return view('public.'.App::getLocale().'.newsroom');
    }

}
