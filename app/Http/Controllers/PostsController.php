<?php

namespace App\Http\Controllers;

use App\Models\Post;

class PostsController extends Controller {

    public function getIndex(){
//        $posts = Post::all();
        $posts = [];
        return view('auth.user.posts.index', compact('posts'));
    }

    public function getNew(){
        return View::make('auth.user.posts.new');
    }
    
    public function postNew()
    {
        
    }

}
