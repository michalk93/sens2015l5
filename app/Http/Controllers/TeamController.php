<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;

class TeamController extends Controller {


    function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    public function getIndex($id = null){
        $companies = Company::where('visible_back', 1)->remember(Config::get('system.cache-time'))->get()->sortBy(function($company){
            return $company->position;
        });

        if(is_null($id)){
            $selected = false;
            $users = $this->users->getVisibleUsers();
        }else{
            $selected = true;
            $users = $this->users->getUserFromComany($id);

        }
        return View::make('auth.user.team', compact('companies', 'selected', 'users'));
    }

    public function getSearch(){
        $companies = Company::where('visible_back', 1)->remember(Config::get('system.cache-time'))->get()->sortBy(function($company){
            return $company->position;
        });

        $searchKey = Input::get('search_key');
        $selected = false;
        $users = User::where('lastname', 'LIKE', "%$searchKey%")->orWhere('firstname', 'LIKE', "%$searchKey%")->orderBy('lastname')->get();
        return View::make('auth.user.team-search', compact('companies', 'users', 'selected'));
    }
}
