<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;

class UserController extends Controller {

    public function getLogin(){
        return view('auth.user.login');
    }

    public function getLogout(){
        Auth::logout();
        return Redirect::route('home');
    }

    public function postLogin(LoginRequest $request){
//        $credentials = [
//            'email' => $request->input('email'),
//            'password'  => $request->input('password'),
//        ];

        if(Auth::attempt($request->all(), true)){
           return Redirect::to('/posts');
        }else {
            return Redirect::back()->with('flashmsg', new FlashMessage("Nieprawidłowe dane logowania.", FlashMessageType::$ERROR));
        }
    }


    public function getIndex(){
        return view('auth.user.posts.index');
    }

    public function getChangePassword(){
        return view('auth.user.password-change');
    }

    public function postChangePassword(){

        $validator = Validator::make(Input::all(), ['current_password' => 'required', 'new_password' => 'required|min:6', 'new_password_confirmation' => 'required|min:6|same:new_password']);

        if($validator->fails()){
            return Redirect::back()->withErrors($validator);
        }
        $user = User::find(Auth::user()->id);

        if(!Hash::check(Input::get('current_password'), $user->getAuthPassword())){
            return Redirect::back()->with('flashmsg', new FlashMessage("Aktualne hasło nie jest prawidłowe.", FlashMessageType::$ERROR));
        }

        $user->password = Hash::make(Input::get('new_password'));
        $user->save();
        return Redirect::action('PostsController@getIndex')->with('flashmsg', new FlashMessage("Hasło zostało zmienione.", FlashMessageType::$SUCCESS));
    }


}
