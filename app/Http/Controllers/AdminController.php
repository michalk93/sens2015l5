<?php

namespace App\Http\Controllers;


class AdminController extends Controller {


    public function getPartners()
    {
        $companies = Company::all()->sortBy(function($company){
            return $company->position;
        });
        return View::make('auth.user.admin.partners', compact('companies'));
    }

    public function getNewPartner(){
        return View::make('auth.user.admin.new-partner');
    }

    public function postNewPartner()
    {
        if(Input::hasFile('logo') && Input::file('logo')->isValid()){
            $filename =  Str::slug(Str::ascii(Str::lower(Input::get('name')))).".".Input::file('logo')->getClientOriginalExtension();
            Input::file('logo')->move(public_path().'/assets/images/partners-logos/', $filename);
            $company = new Company();

            $company->name = Input::get('name');
            $company->url = Input::get('site_url');
            $company->logo = $filename;
            $company->position = -1;
            $company->save();
            $company->position = $company->id;
            $company->save();
            return Redirect::action('AdminController@getPartners');
        }

        return Redirect::back();
    }



    public function getPartnerPositionUp($id){
        $company = Company::find($id);
        $companyPrev = Company::where('position', $company->position-1)->get();
        if($companyPrev->isEmpty()){
            return Redirect::back();
        }
        $companyPrev = $companyPrev->first();
        $position = $companyPrev->position;
        $companyPrev->position = $company->position;
        $company->position = $position;
        $company->save();
        $companyPrev->save();

        return Redirect::back();
    }

    public function getPartnerPositionDown($id){
        $company = Company::find($id);
        $companyPrev = Company::where('position', $company->position+1)->get();
        if($companyPrev->isEmpty()){
            return Redirect::back();
        }
        $companyPrev = $companyPrev->first();
        $position = $companyPrev->position;
        $companyPrev->position = $company->position;
        $company->position = $position;
        $company->save();
        $companyPrev->save();

        return Redirect::back();
    }

    //-------------------------------------------
    // REPRESENTATIVE's SECTION
    //-------------------------------------------

    public function getNewRepresentative(){
        $companies = Company::all();
        return View::make('auth.user.admin.new-representative', compact('companies'));
    }

    public function postNewRepresentative(){
        $user = new User();
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->position = Input::get('position');
        $user->title = Input::get('title');
        $user->email = Input::get('email');
        $user->description = Input::get('description');
        $password = Str::random(8);
        Event::fire('auth-data.update', [$user, $password]);
        $user->password = Hash::make($password);
        $user->phone = Input::get('phone');
        $user->companies_id = Input::get('companies_id');
        $user->save();

       // Event::fire('representative.send', array($user, $password));

        return Redirect::action('PostsController@getIndex');
    }

    //-------------------------------------------
    // USERS' AVATARS SECTION
    //-------------------------------------------

    public function getAvatar($id){
        return View::make('auth.user.admin.new-avatar')->with('id', $id);
    }

    public function postAvatar(){
        if(Input::hasFile('avatar')){
            $user = User::find(Input::get('id'));
            $filename = "user".$user->id.".".Input::file('avatar')->getClientOriginalExtension();
            Input::file('avatar')->move(public_path().'/assets/avatars/', $filename);
            $user->avatar = $filename;
            $user->save();
        }
        return Redirect::action('TeamController@getIndex');
    }
}
