<?php


//
//Route::get('partnerzy/{partner}', function($partner){
//    try {
//        return View::make('public.partners.' . $partner);
//    }catch(InvalidArgumentException $e){
//        return Redirect::to('/#partnerzy');
//    }
//});
//
//Route::get('language/{lang}', ['as' => 'language', 'uses' => function($lang){
//    Session::put('language', $lang);
//    return Redirect::intended();
//}]);
//
//Route::get('/mail/{id}', function($id){
//    $user = User::find($id);
//    if(!$user){
//        return "No email send. User doesn't exists";
//    }
//    $dataAuth = file(public_path()."/auth-data.txt");
//    foreach($dataAuth as $key => $value){
//        $userData = explode(",",$value);
//        if($userData[3] == $user->email){
//            $password = $userData[4];
//            try{
//                Event::fire('representative.send', compact('user', 'password'));
//            }catch(Exception $e) {
//                return $e->getMessage();
//            }
//            $user->email_send = 1;
//            $user->save();
//            return "Email send to: ".$user->firstname." ".$user->lastname." - ".$user->email;
//        }
//    }
//
//    return "No email send. Email address can be incorrect or user with this email doesn't exist";
//
//    //return View::make('emails.representative-info',['user' => $user, 'password' => $user->password]);
//});
//
//Route::get('login', ['as' => 'login', 'uses' => 'UserController@getLogin']);
//Route::post('login', 'UserController@postLogin');
//
/**
 * USER AUTHENTICATION AREA
 */

// Authentication routes...
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('/login', ['as' => 'login-post', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);



Route::group(['middleware' => 'auth'], function(){
    Route::controller('user', 'UserController');
    Route::controller('admin', 'AdminController');
    Route::controller('posts', 'PostsController');
    Route::controller('team', 'TeamController');
});

//Route::controller('/', 'PagesController');
Route::get('/',['as' => 'home', 'uses' => "PagesController@getIndex"]);

Route::group(['prefix' => 'projekty'], function(){
    Route::get('/', ['as' => 'projects', 'uses' => 'PagesController@getProjects']);
});

Route::group(['prefix' => 'fundacja'], function(){
    Route::get('/', ['as' => 'foundation', 'uses' => 'PagesController@getFoundation']);
});

Route::group(['prefix' => 'partnerzy'], function(){
    Route::get('/', ['as' => 'partners', 'uses' => 'PagesController@getPartners']);
});

Route::group(['prefix' => 'newsroom'], function(){
    Route::get('/', ['as' => 'newsroom', 'uses' => 'PagesController@getNewsroom']);
});
//
////App::missing(function($exception)
////{
////    return Redirect::to('/');
////});