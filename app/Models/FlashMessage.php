<?php namespace App\Models;

/**
 * Class FlashMessage
 */
class FlashMessage {

    /**
     * Contains text message to display for user
     * @var null
     */
    private $message = null;

    /**
     * Type of message defined by bootstrap. One of following: 'default', 'success', 'info', 'warning'
     * @var null
     */
    private $type = null;

    /**
     * @param $message
     * @param null $type
     */
    function __construct($message, $type = null)
    {
        $this->message = $message;
        $this->type = $type or "default";
    }

    /**
     * Return text of message
     * @return null
     */
    function getMessage()
    {
        return $this->message;
    }

    /**
     * Return type of message
     * @return null
     */
    function getType(){
        return $this->type;
    }


}


class FlashMessageType {
    public static $INFO = 'blue';
    public static $WARNING = 'orange';
    public static $ERROR = 'red';
    public static $SUCCESS = 'green';
}