<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $fillable = ['topic', 'content', 'tags', 'type'];

    public static $rules = [
        'topic' => 'required|min:10',
        'content' => 'required|min:20',
        'type' => 'required'
    ];

}
