<?php

namespace App\Console\Commands;

use App\Events\UserPasswordSet;
use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SetUserNewPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:newpassword {email : Email address of user to generate and send new password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new password and send an email with authorization data to user specified by given email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $user = User::where('email', $email)->first();
        if($user === null){
            $this->error('User with given email was not found!');
            return false;
        }
        $password = Str::random(8);
        $user->password = Hash::make($password);
        $user->save();
        try {
            Event::fire(new UserPasswordSet($user, $password));
        }catch(Exception $e) {
            $this->error('There was an error while sending email!');
            return $e->getTraceAsString();
        }

        $userinfo = $user->firstname.' '.$user->lastname.' <'.$user->email.'>';
        $this->info('Password changed. Email with new password was sent to '.$userinfo);
        return true;
    }
}
