<?php

namespace App\Repositories;

use App\Repositories\Eloquent\Repository;

class UserRepository extends Repository {

    function getVisibleUsers()
    {
        return User::where('visible', 1)
            ->remember(Config::get('system.cache-time'))
            ->get()->shuffle();
    }

    function getUserFromComany($id)
    {
        return User::where('visible', 1)
            ->where('companies_id', $id)
            ->orderBy('priority')
            ->remember(Config::get('system.cache-time'))
            ->get();
    }

    function getUsersGroupedByCompany()
    {
        $users = User::where('visible', 1)->with('company')->remember(Config::get('system.cache-time'))->get();
        $groups = $users->sortBy(function($user) use ($users){
            return (is_null($user->company)) ? $users->count()+1 : $user->company->position;
        })->groupBy('companies_id');
        return $groups;
    }

    function model()
    {
        return 'App\User';
    }
}