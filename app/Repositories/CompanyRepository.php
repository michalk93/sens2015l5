<?php

namespace App\Repositories;

use App\Repositories\Eloquent\Repository;
use App\Models\Company;

class CompanyRepository extends Repository {



    private function getCompaniesGroup($group_name){
        return Company::where('visible_front', 1)
            ->where("group_name", $group_name)
            ->remember(Config::get('system.cache-time'))
            ->get()->sortBy(function($company){
                return $company->position;
            });
    }

    public function getPartners()
    {
        return $this->getCompaniesGroup("PARTNERS");
    }

    public function getTeam()
    {
        return $this->getCompaniesGroup("TEAM");
    }

    public function all($columns = array('*'))
    {
        return Company::where('visible_front', 1)
            ->get($columns)->sortBy(function($company){
                return $company->position;
            });
    }

    function model()
    {
        return 'App\Models\Company';
    }
}