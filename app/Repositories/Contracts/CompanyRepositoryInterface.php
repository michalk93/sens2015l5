<?php

interface CompanyRepositoryInterface {
    function getPartners();
    function getTeam();
    function all();
}