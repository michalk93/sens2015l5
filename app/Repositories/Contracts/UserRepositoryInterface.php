<?php

interface UserRepositoryInterface {
    function getVisibleUsers();
    function getUserFromComany($id);
    function getUsersGroupedByCompany();
}