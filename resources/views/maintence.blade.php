<!doctype html>
<html class="no-js" lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- Place favicon.ico in the root directory -->
    {{HTML::style('assets/css/materialize.min.css')}}
    {{HTML::style('assets/css/public/main.css')}}

    {{--{{HTML::script('assets/js/modernizr.js')}}--}}

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="row">
    <div class="col s12 m6 offset-m3">
        <div class="row">
            <div class="col s12">
                <div class="card grey darken-3">
                    <div class="card-content white-text">
                        <span class="card-title">SENS 2015</span>
                        <p>Przepraszamy, ale serwis nie jest w tej chwili dostępny z uwagi na ciągłą pracę nad nim :)</p>
                        <p>Pytania, propozycje, sugestie na adres: michalkolbusz@mailplus.pl</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
{{HTML::script('assets/js/materialize.min.js')}}
{{HTML::script('assets/js/main.js')}}

</body>
</html>