@extends('auth.master')

@section('content')
    <div class="row form-admin">
        <div class="s12">
            <div class="row">
                <div class="s12"><h5>Dodawanie przedstawiciela</h5></div>
            </div>
            <div class="row">
                {{Form::open(['class' => 'col s12'])}}
                    <div class="row">
                        <div class="input-field col s4">
                            <input id="title" type="text" class="validate" name="title">
                            <label for="title">Tytuł</label>
                        </div>
                        <div class="input-field col s4">
                            <input id="first_name" type="text" class="validate" name="firstname">
                            <label for="first_name">Imię</label>
                        </div>
                        <div class="input-field col s4">
                            <input id="last_name" type="text" class="validate" name="lastname">
                            <label for="last_name">Nazwisko</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <label>Partner/firma</label>
                            <select name="companies_id">
                                <option value="" disabled selected>Wybierz firmę/partnera</option>
                                @foreach($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="position" type="text" class="validate" name="position">
                            <label for="position">Pozycja/stanowisko</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="description" type="text" class="validate" name="description">
                            <label for="description">Czym się zajmuje</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="email" type="email" class="validate" name="email">
                            <label for="email">Email</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="phone" type="text" class="validate" name="phone">
                            <label for="phone">Telefony</label>
                        </div>
                    </div>

                    <div class="row right">
                        <div class="s12">
                            <button class="btn waves-effect waves-light" type="submit" name="action">Zapisz
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                {{Form::close()}}
            </div>
        </div>
    </div>

@stop

@section('js')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
@stop