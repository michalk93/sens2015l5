@extends('auth.master')

@section('content')
    <div class="row">
        <div class="s12">
            <div class="row">
                <div class="s12"><h5>Dodawanie avatara przedstawiciela</h5></div>
            </div>
            <div class="row">
                <div class="col s12">
                    {{Form::open(['files' => true])}}
                    <div class="row">
                        <div class="file-field input-field s12">
                            <input class="file-path validate" type="text"/>
                            <div class="btn">
                                <span>Avatar</span>
                                <input type="file" name="avatar" />
                            </div>
                        </div>
                    </div>
                    {{Form::hidden('id', $id)}}
                    <div class="row right">
                        <div class="s12">
                            <button class="btn waves-effect waves-light" type="submit" name="action">Zapisz
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>

@stop