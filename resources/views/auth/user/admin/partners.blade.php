@extends('auth.master')

@section('content-title')
    Lista partnerów
@stop

@section('content')
    <div class="row">
        <div class="s12">
            <div class="row">
                <table>
                    <thead>
                    <tr>
                        <th data-field="id">Nazwa</th>
                        <th data-field="name">Pozycja</th>
                        <th data-field="price">Menu</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <td><a href="{{action('AdminController@getPartnerPositionUp', $company->id)}}"><i class="mdi-hardware-keyboard-arrow-up"></i></a>
                                <a href="{{action('AdminController@getPartnerPositionDown', $company->id)}}"><i class="mdi-hardware-keyboard-arrow-down"></i></a> {{$company->name}}</td>
                            <td>{{$company->position}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop