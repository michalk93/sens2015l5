@extends('auth.master')

@section('content-title')
    Dodawanie partnera
@stop

@section('content')
<div class="row">
    <div class="s12">
        <div class="row">
            <div class="s12"><h5></h5></div>
        </div>
        <div class="row">
            <div class="col s12">
            {{Form::open(['files' => true])}}
            <div class="row">
                <div class="input-field col s12">
                    <input id="name" type="text" class="validate" name="name">
                    <label for="name">Nazwa</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="site_url" type="text" class="validate" name="site_url">
                    <label for="site_url">Adres strony internetowej</label>
                </div>
            </div>
            <div class="row">
                <div class="file-field input-field s12">
                    <input class="file-path validate" type="text"/>
                    <div class="btn">
                        <span>Logo</span>
                        <input type="file" name="logo" />
                    </div>
                </div>
            </div>
            <div class="row right">
                <div class="s12">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Zapisz
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
            {{Form::close()}}
            </div>
        </div>
    </div>
</div>

@stop