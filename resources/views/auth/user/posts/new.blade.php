@extends('auth.master')

@section('content-title')
    Nowa wiadomość
@stop

@section('content')
    <div class="row">
        <div class="s12">
            <div class="row">
                <div class="col s12">
                    {{Form::open()}}
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="title" type="text" class="validate" name="title">
                            <label for="title">Tytuł</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="content" class="materialize-textarea" name="content"></textarea>
                            <label for="content">Treść wiadomości</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="s12 center">
                            <button class="btn waves-effect waves-light" type="submit" name="action">Zapisz
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>

@stop