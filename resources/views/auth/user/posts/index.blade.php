@extends('auth.master')

@section('content')
    <h5>Wiadomości</h5>
    @foreach($posts as $post)
        <div class="row" style="border-left: 10px red solid">
            <div class="col s12 m12 l2">
                <h5>{{$post->author->firstname." ".$post->author->lastname}}</h5>
                <p>2{{$post->created_at}}</p>
                <span class="badge red white-text left">Ważne!</span>
            </div>
            <div class="col s12 m12 l10">
                <p>Tytuł</p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda atque beatae maxime omnis, perferendis quidem sed tempore. Autem corporis cupiditate necessitatibus nulla obcaecati quo similique tempora, voluptas. Est, quo.</p>
            </div>
        </div>
    @endforeach

@stop