@extends('auth.master')

@section('content-title')
    Zmiana hasła
@stop

@section('content')
    <div class="row">
        <div class="col s12">
            <h5>Wypełnij poniższy formularz, aby zmienić aktualne hasło</h5>
        </div>
    </div>
    @include('auth.user.parts.flashmsg')
    <div class="row">
        {{Form::open(['class' => 'col s12'])}}

            <div class="row">
                <div class="input-field col s12">
                    <input id="current_password" type="password" class="validate" name="current_password">
                    <label for="current_password">Aktulane hasło:</label>
                    @if($errors->has('current_password'))
                        <p class="red-text">{{$errors->first('current_password')}}</p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="new_password" type="password" class="validate" name="new_password">
                    <label for="new_password">Nowe hasło</label>
                    @if($errors->has('new_password'))
                        <p class="red-text">{{$errors->first('new_password')}}</p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="new_password_confirmation" type="password" class="validate" name="new_password_confirmation">
                    <label for="new_password_confirmation">Powtórz nowe hasło</label>
                    @if($errors->has('new_password_confirmation'))
                        <p class="red-text">{{$errors->first('new_password_confirmation')}}</p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s12 center">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Zmień hasło
                        <i class="mdi-content-send right"></i>
                    </button>
                </div>
            </div>
        {{Form::close()}}
    </div>
@stop