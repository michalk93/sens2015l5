@if(Session::has('flashmsg'))
    <div class="row">
        <div class="col s12">
            <div class="card-panel {{Session::get('flashmsg')->getType()}}">{{Session::get('flashmsg')->getMessage()}}</div>
        </div>
    </div>
@endif