<div class="navbar-fixed" id="top-nav-wrapper">
    <nav class="grey darken-3 text-white" role="navigation">
        <div class="container">
            <div class="nav-wrapper">
                <span class="brand-logo">SENS 2015</span>
                <ul class="right hide-on-med-and-down uppercase" id="menu-main">
                    <li><a href="{{action('PostsController@getIndex')}}">Wiadomości</a></li>
                    <li><a href="{{action('TeamController@getIndex')}}">Zespół</a></li>
                    <li><a href="{{action('UserController@getChangePassword')}}">Zmiana hasła</a></li>
                    @if(Auth::user()->isAdmin())
                    <li><a id="dropdown-button" href="#!" data-activates="dropdownadmin">Administracja<i class="mdi-navigation-arrow-drop-down right"></i></a></li>
                    <ul id="dropdownadmin" class="dropdown-content">
                        <li><a href="{{action('AdminController@getPartners')}}" class="collection-item">Partnerzy</a></li>
                        <li><a href="{{action('AdminController@getNewPartner')}}" class="collection-item">Dodaj partnera/firmę</a></li>
                        <li class="divider"></li>
                        <li><a href="{{action('AdminController@getNewRepresentative')}}" class="collection-item">Dodaj przedstawiciela</a></li>
                    </ul>
                    @endif
                    <li><a class="prevent" href="{{route('logout')}}">Wyloguj</a></li>
                </ul>

                <ul id="nav-mobile" class="side-nav uppercase"></ul>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            </div>
        </div>
    </nav>

</div>

@section('js')
    @parent
    <script type="text/javascript">
        $("#nav-mobile").html($('#menu-main').html());
        $("#dropdown-button").dropdown();
    </script>
@stop