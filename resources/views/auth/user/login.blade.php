<!doctype html>
<html class="no-js" lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- Place favicon.ico in the root directory -->
    {!! HTML::style('assets/css/materialize.min.css') !!}
    {!! HTML::style('assets/css/user/main.css') !!}

    {!! HTML::script('assets/js/modernizr.js') !!}
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
{!! HTML::script('assets/js/materialize.min.js') !!}

<div class="row">
    <div class="col s12">
        @include('auth.user.parts.flashmsg')
        <div class="row">
            <div class="col s12 valign-wrapper">
                <div class="card grey valign darken-3">
                    {!! Form::open() !!}
                    <div class="card-content white-text">
                        <span class="card-title">Logowanie</span>
                        <div class="row">
                            <div class="input-field col s12 text-white">
                                <input id="email" type="text" name="email">
                                <label for="email" class="">Email</label>
                                @if($errors->has('email'))
                                    <p class="text-red">{{$errors->first('email')}}</p>
                                @endif
                            </div>
                            <div class="input-field col s12">
                                <input id="password" type="password" name="password">
                                <label for="password">Hasło</label>
                                @if($errors->has('password'))
                                    <p class="text-red">{{$errors->first('password')}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-action row">
                        <div class="col s12">
                            <button class="btn waves-effect waves-light col s12" type="submit" name="action">Zaloguj
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col s12 center-align">
        <p>Potrzebujesz pomocy lub informacji związanych ze stroną?</p>
        <p>Pisz do mnie: michalkolbusz@mailplus.pl</p>
    </div>
</div>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</body>
</html>