@extends('auth.master')

@section('content-title')
    Członkowie zespołu/przedstawiciele partnerów
@stop

@section('content')
    <div class="row">
        <p>Wybierz firmę, aby wyświetlić jej przedstawicieli lub wyszukaj osobę</p>
        <div class="row">
            <div class="col s12">
                {{Form::open(['action' => 'TeamController@getSearch', 'method' => 'GET'])}}
                <div class="row">
                    <div class="input-field col s6 offset-s3 center-align">
                        <input id="search_team" type="text" class="validate" name="search_key">
                        <input type="submit" class="btn grey darken-3" value="Szukaj"/>
                        <label for="search_team">Szukaj (wpisz imię lub nazwisko osoby)</label>
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
        <div class="input-field col s12 center-align">
            <select>
                <option value="" disabled selected>Choose your option</option>
                @foreach($companies as $company)
                    <option value="{{$company->id}}"><a href="{{action('TeamController@getIndex', $company->id)}}" class="waves-effect waves-light btn grey darken-3 col s12 m6 l4">{{$company->name}}</a></option>
                @endforeach
                <label>Wybierz partnera z listy rozwijanej</label>
            </select>
        </div>

    </div>
    @if($users->isEmpty())
        <div class="row">
            <div class="col s12">
                <p>Brak rezultatów wyszukiwania.</p>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col s12">
                @foreach(array_chunk($users->all(), 2) as $users_chunk)
                    <div class="row">
                        @foreach($users_chunk as $user)
                            <div class="col s12 m6">
                                <div class="row">
                                    <div class="col s12 m3 l4">
                                        <div class="row">
                                            <div class="col s12 center">
                                                {{HTML::image('/assets/avatars/'.$user->avatar, '',['class' => 'responsive-img', 'style' => 'max-height: 150px'])}}
                                                @if(Auth::user()->isAdmin())
                                                    <div><a href="{{action('AdminController@getAvatar', $user->id)}}"><i class="mdi-content-add-box"></i></a></div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12 m9 l8 grey-text text-darken-4" style="font-size: 12px;">
                                        <div class="row">
                                            <div class="col s12">
                                                <p>{{$user->title}}
                                                    <span style="font-size: 14px;"><strong>{{$user->firstname." ".$user->lastname}}</strong></span><br/>
                                                    <strong>{{$user->description}}</strong>
                                                </p>
                                                <p>{{$user->position}}</p>
                                                <div>

                                                    <p>
                                                        @foreach(explode(',',$user->phone) as $number)
                                                            @if(!empty($number)){{$number}}<br/>@endif
                                                        @endforeach
                                                        @foreach(explode(',', $user->email) as $email)
                                                            e: {{$email}}<br/>
                                                        @endforeach

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@stop