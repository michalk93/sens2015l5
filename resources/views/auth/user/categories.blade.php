@extends('auth.master')

@section('content-title')
    Kategorie
@stop

@section('content')
    <div class="row">
        <div class="col s12">
            <div class="collection">
            @foreach($categories as $category)
                <a href="" class="collection-item">{{$category->name}}</a>
            @endforeach
            </div>
        </div>
    </div>
@stop