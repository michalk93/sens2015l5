<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>SENS 2015</title>
</head>
<body>
    <div style="color: #424242; font-family: Verdana, sans-serif">
        <h3 style="padding: 20px; border-bottom: 1px #424242 solid">SENS 2015</h3>
        <div style="margin-top: 20px;">
            <p>Witaj {{$firstname}} {{$lastname}},<br/>
                poniżej znajdują się dane do logowania w panelu użytkownika projektu <em>SENS 2015</em></p>
            <p>Adres www: <a href="http://sens2015.pl/login">www.sens2015.pl/login</a></p>
            <p>Login/Email: {{$email}}</p>
            <p>Hasło: {{$password}}</p>
            <p>Powyższe hasło można zmienić po zalogowaniu w panelu.</p>

            <p>Wszelkie uwagi na temat funkcjonowania strony (błędów, propozycji zmian, nowych funkcjonalności) prosimy kierować na adres email: <em>michalkolbusz@mailplus.pl</em> (można też automatycznie odpowiedzieć na tę wiadomość)</p>
            <p>Ta wiadomość została wysłana automatycznie przez serwis <a href="http://www.sens2015.pl">sens2015.pl</a>.</p>

        </div>
    </div>
</body>
</html>