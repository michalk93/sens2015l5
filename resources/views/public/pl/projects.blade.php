@extends('public.master')

@section('content')
    <div class="container" id="foundation-view" style="margin-top: 112px;">
        <div class="row">
            Projects
           </div>
    </div>
@stop

@section('css')
    @parent
    {!! HTML::style('assets/css/jquery.fullpage.min.css') !!}
@stop

@section('js')
    @parent
    {!! HTML::script('assets/js/jquery.fullpage.min.js') !!}
    <script>
        $('.lazy').Lazy({
            customLoaderName: function(element) {
                element.load();
            },
        });

        $('#submenu').fadeIn('slow')

    </script>
@stop