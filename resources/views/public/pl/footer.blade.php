<footer class="page-footer no-print grey darken-3">
    <div class="container">
        <div class="row">
            <div class="col l6 s12 white-text">
                <h5>Kontakt</h5>
                <h6>email: biuro@sens2015.pl</h6>
            </div>
            {{--<div class="col l4 offset-l2 s12">--}}
                {{--<h5 class="white-text">Links</h5>--}}
                {{--<ul>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Copyright © {!! link_to_action('PagesController@getIndex','SENS 2015') !!}
        </div>
    </div>
</footer>