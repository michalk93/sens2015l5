@extends('public.master')

@section('content')
<div class="container" id="foundation-view" style="margin-top: 112px;">
    <div class="row"  style="text-align: justify">
        <div class="col s12 m5">
            <h3 class="center-align" style="font-weight: 200">O NAS</h3>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aspernatur atque commodi, delectus distinctio enim error et explicabo facilis id ipsum, maxime nesciunt numquam odio omnis perferendis repudiandae, rerum saepe?</span><span>Adipisci atque deserunt et molestiae nihil odit quam quas quia ratione repellat, repellendus tempore velit? Accusamus adipisci, aliquam consequuntur culpa cumque deleniti fugiat impedit modi perferendis quisquam sint, tempore temporibus.</span><span>Aperiam aut consequuntur corporis debitis delectus distinctio dolor ea et fugit harum id illo in laborum minima molestias numquam optio perspiciatis provident quis quisquam sunt suscipit veniam, voluptates? Beatae, quo.</span><span>Accusantium commodi dolore doloribus enim, excepturi impedit iure nostrum omnis quia rem! Ab aut facilis minus, molestias nesciunt repudiandae? Animi cum delectus doloremque minima modi molestiae neque nihil nobis qui!</span><span>Architecto corporis inventore praesentium repellat repudiandae. Inventore maxime numquam repellat veritatis? Autem ducimus ea earum eius, eum explicabo facere harum, in inventore ipsa magni maiores minus, praesentium ratione reiciendis repudiandae.</span></p>
        </div>
        <div class="col s12 m5 offset-m2">
            <h3 class="center-align" style="font-weight: 200">MISJA</h3>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aspernatur atque commodi, delectus distinctio enim error et explicabo facilis id ipsum, maxime nesciunt numquam odio omnis perferendis repudiandae, rerum saepe?</span><span>Adipisci atque deserunt et molestiae nihil odit quam quas quia ratione repellat, repellendus tempore velit? Accusamus adipisci, aliquam consequuntur culpa cumque deleniti fugiat impedit modi perferendis quisquam sint, tempore temporibus.</span><span>Aperiam aut consequuntur corporis debitis delectus distinctio dolor ea et fugit harum id illo in laborum minima molestias numquam optio perspiciatis provident quis quisquam sunt suscipit veniam, voluptates? Beatae, quo.</span><span>Accusantium commodi dolore doloribus enim, excepturi impedit iure nostrum omnis quia rem! Ab aut facilis minus, molestias nesciunt repudiandae? Animi cum delectus doloremque minima modi molestiae neque nihil nobis qui!</span><span>Architecto corporis inventore praesentium repellat repudiandae. Inventore maxime numquam repellat veritatis? Autem ducimus ea earum eius, eum explicabo facere harum, in inventore ipsa magni maiores minus, praesentium ratione reiciendis repudiandae.</span></p>
        </div>
    </div>

    <div class="row" style="margin-top: 15em">
        <div class="col s12">
            <h3 class="center-align" style="font-weight: 200">ZESPÓŁ</h3>
        @foreach(array_chunk($team, 4) as $teamRow)
            <div class="row">
            @foreach($teamRow as $teamMate)
                <div class="col s12 m3 center-align">
                    <img class="lazy" style="border-radius: 50%;" src="{{$teamMate->avatarUrl}}" alt="Contact Person">
                    <h5>{{$teamMate->firstname}} {{$teamMate->lastname}}</h5>
                    <p>{{$teamMate->description}}</p>
                </div>
            @endforeach
            </div>
        @endforeach
        </div>
    </div>

    <div class="row" style="margin-top: 15em">
        <div class="col s12">
            <h3 class="center-align" style="font-weight: 200">STATUT</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid deleniti iure maiores nobis praesentium? Ab accusamus expedita facere impedit, ipsa iure laudantium, molestias optio perspiciatis quos repudiandae temporibus ut velit?</p>
            <p><a href="#">Statut do pobrania</a></p>
        </div>
    </div>

    <div class="row" style="margin-top: 15em">
        <div class="col s12">
            <h3 class="center-align" style="font-weight: 200">WSPARCIE</h3>

        </div>
    </div>

    <div class="row" style="margin-top: 15em">
        <div class="col s12">
            <h3 class="center-align" style="font-weight: 200">KONTAKT</h3>
            <div class="row">
                <div class="col s12">
                    <address>
                        FUNDACJA S.E.N.S
                        ul. Cystersów<br />
                        00-000 Kraków
                    </address>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    @parent
    {!! HTML::style('assets/css/jquery.fullpage.min.css') !!}
@stop

@section('js')
    @parent
    {!! HTML::script('assets/js/jquery.fullpage.min.js') !!}
    <script>
        $('#submenu').fadeIn('slow')

    </script>
@stop