@extends('public.master')

@section('content')
    <div class="container" id="foundation-view" style="margin-top: 112px;">
        <div class="row">
            <div class="col s12 l6">
                <div class="fb-page" data-href="https://www.facebook.com/bncdn" data-tabs="timeline,events,messages" data-width="500" data-height="800" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
                    <div class="fb-xfbml-parse-ignore"></div>
                </div>
            </div>
            <div class="col s12 l6"></div>
        </div>
    </div>
@stop

@section('css')
    @parent
    {!! HTML::style('assets/css/jquery.fullpage.min.css') !!}
@stop

@section('js')
    @parent
    {!! HTML::script('assets/js/jquery.fullpage.min.js') !!}
    <script>
        $('.lazy').Lazy({
            customLoaderName: function(element) {
                element.load();
            },
        });

        $('#submenu').fadeIn('slow')

    </script>
@stop