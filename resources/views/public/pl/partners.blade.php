@extends('public.master')

@section('content')
    <div class="container" id="partners-view" style="margin-top: 112px;">
        <div class="row">
            <h3>PARTNERZY</h3>
            <div class="col s12">
                @foreach($partners as $partnersChunk)
                    <div class="row">
                        @foreach($partnersChunk as $partner)
                            <div class="col l4 m2 s12">
                                <div class="card">
                                    <div class="lazy activator partner-item card-image waves-effect waves-block waves-light center-align" style="background-image: url(''); background-size: {{$partner->logo_scale}}%;" data-src="{{asset('assets/images/partners-logos/')}}/{{{$partner->logo or 'noimage.png'}}}">
                                        {{--<img height="100" class="activator" src="{{asset('assets/images/partners-logos/'.$partner->logo)}}">--}}
                                    </div>
                                    <div class="card-content">
                                        <span class="card-title activator grey-text text-darken-4">{{$partner->name}}<i class="right">więcej...</i></span>
                                        <p><a href="{{$partner->url}}" target="_blank">Strona WWW</a></p>
                                    </div>
                                    <div class="card-reveal">
                                        <span class="card-title grey-text text-darken-4">{{$partner->name}}<i class="material-icons right">close</i></span>
                                        <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A asperiores, aspernatur at commodi delectus deleniti deserunt exercitationem facere ipsum, modi nulla perspiciatis possimus ratione tempora vel. Distinctio hic perferendis quia.</span><span>Ad assumenda beatae debitis dolor doloribus error esse explicabo facilis fuga hic illo laudantium modi nam numquam obcaecati perferendis perspiciatis quas rerum saepe sapiente similique sint, tempora, ut veritatis voluptatem.</span></p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop

@section('css')
    @parent
    {!! HTML::style('assets/css/jquery.fullpage.min.css') !!}
@stop

@section('js')
    @parent
    {!! HTML::script('assets/js/jquery.fullpage.min.js') !!}
    <script>
        $('.lazy').Lazy({
            effect: "fadeIn",
            effectTime: 500,
        });

    </script>
@stop