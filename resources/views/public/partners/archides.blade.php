@extends('public.master')

@section('title')
    Sens 2015 - Archides
@stop

@section('content')
    <div class="center-align">
        <p>{{HTML::image('assets/images/partners-logos/archides.jpg', "Archides",['width' => 300])}}</p>
        <p><strong>Architekt Wojciech Korbel</strong></p>
        <p><strong>Architekt Jarosław Huebner</strong></p>
        <br/>

        <p>Proszowicka 10, 31-228 Kraków</p>
        <p>tel./fax. (12) 632-53-02</p>
        <p>604-190-361, 693-079-324</p>
        <p>e-mail: archides@poczta.onet.pl </p>

    </div>
@stop
