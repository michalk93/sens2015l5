@extends('public.master')

@section('title')
    Jurkowski Design - Sens 2015
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">

                <p class="right-align" style="border-bottom: 1px #464646 solid; margin-top: 20px">
                Architekt, projektant, rzeźbiarz
                </p>
                <div class="row">
                    <div class="col s12 m6">
                        <img src="{{asset('assets/images/jurkowski-book.jpg')}}" alt="" class="responsive-img"/>
                    </div>
                    <div class="col s12 m6">
                        <p>
                            Projektant miejskiej przestrzeni publicznej <br/> dążący do zaistnienia w niej wielkogabarytowej sztuki.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
