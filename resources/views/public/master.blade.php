<!doctype html>
<html class="no-js" lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta name="description" content="S.E.N.S - Super Energia Na S.E.N.S, czyli wydarzenie, którego celem jest ingerencja kultury i sztuki w przestrzeń miasta. S.E.N.S ma stanowić zdecydowany i wyrazisty przykład prezentacji wielkogabarytowej sztuki w przestrzeni miasta.">
    <meta name="keywords" content="S.E.N.S, sens2015, maciej jurkowski, sztuka, rzeźba miejska, kraków, plac szczepański, kultura, sens"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Place favicon.ico in the root directory -->
    @yield('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
    {{--{{HTML::style('assets/css/transitions.css')}}--}}
    {!! HTML::style('assets/css/public/main.css') !!}

    {{--{{HTML::script('assets/js/modernizr.js')}}--}}

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->


</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.5&appId=503619179749987";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
{{--<div id="lang-switcher" style="">--}}
    {{--<div class=""><a href="{{route('language','pl')}}"><p>PL</p></a></div>--}}
    {{--<div class=""><p>EN</p></div>--}}
    {{--<div class=""><p>DE</p></div>--}}
    {{--<div class=""><p>FR</p></div>--}}
    {{--<div class=""><a href="{{route('language','es')}}"><p>ES</p></a></div>--}}
{{--</div>--}}
@include ('public.parts.navigation')
@yield('content')
@include('public.'.App::getLocale().'.footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.6.5/jquery.lazy.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>

<script type="text/javascript" async>
    $('.button-collapse').sideNav();
</script>

{!! HTML::script('assets/js/CookieAlert-latest.min.js', ['async']) !!}

<!-- Hotjar Tracking Code for sens2015.pl -->
<script async>
    (function(f,b){
        var c;
        f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
        f._hjSettings={hjid:37584, hjsv:4};
        c=b.createElement("script");c.async=1;
        c.src="//static.hotjar.com/c/hotjar-"+f._hjSettings.hjid+".js?sv="+f._hjSettings.hjsv;
        b.getElementsByTagName("head")[0].appendChild(c);
    })(window,document);
</script>
@yield('js')

</body>
</html>