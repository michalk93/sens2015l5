@extends('public.master')

@section('title')
    Sens 2015
@stop

@section('content')

<div id="home" class="no-print" data-parallax="scroll" data-image-src="{{asset('assets/images/plac-szczepanski/01.jpg')}}">
    <div class="section no-pad-bot">
        <div class="container">
            {{--<div class="row center">
                <h5 class="header col s12 grey-text">11 kwietnia 2015</h5>
            </div>
            <div class="row center center-align">
                <div class="col s12 ">
                    <a class="btn grey darken-3" href="#">Zobacz program</a>
                </div>
            </div>--}}
        </div>
    </div>
</div>

<div class="row no-mar-bot no-print" id="bottomBar">
    <div class="col s12 grey darken-3">
        <p style="font-size: 2.1em" class="center white-text center-align" >Kraków{{--, Plac Szczepański--}}</p>
    </div>
</div>

<div class="row no-mar-bot full-height-panel" id="sens" style="background-color: #000; padding-top: 64px;">
    <div class="col s12">
        <div class="container">
            <div class="row no-mar-bot">
                <div class="col s12">
                    @include('public.'.App::getLocale().'.manifest')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper no-print full-height-panel" id="partnerzy" style="padding-top: 64px;">
    <div class="container">
        <div class="section">
            <div class="row">
                <div class="col s12">
                    <div class="center header">
                        <h5>PARTNERZY</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($partners as $partner)
                    <a href="{{$partner->url}}" data-src="{{asset('assets/images/partners-logos/')}}/{{{$partner->logo or 'noimage.png'}}}" class="partner-item col s6 m3 l2 lazy" target="_blank" style="background-image: url(''); background-size: {{$partner->logo_scale}}%;"></a>
                @endforeach
            </div>
        </div>
    </div>
</div>


@stop


@section('js')
    @parent
    {{--<script src="{!! asset('assets/js/jquery.lazyload.js') !!}"></script>--}}
    <script src="{!! asset('assets/js/parallax.min.js') !!}"></script>
    <script type="text/javascript">
        (function($){
            $(document).ready(function(){
                $('a.prevent').bind('click', function(ev){
                    ev.preventDefault();
                });

                $(".link").click(function() {
                    $('html, body').stop(false,false).animate({
                        scrollTop: $($(this).data('target-id')).offset().top - $('.navbar-fixed').height()
                    }, 2000);
                });
            }); // end of document ready


            var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
            var image = $('#home');
            var topNavWrapper = $("#top-nav-wrapper").height();
            var bottomBar = $("#bottomBar").height();

            if($("#home").height() < image.height() && image.height() < (viewportHeight - bottomBar - topNavWrapper)){
                $("#home").css({"height": image.height()});
            }else{
                $("#home").css({"height": viewportHeight- bottomBar - topNavWrapper});
            }

            var freeViewPortHeight = viewportHeight-topNavWrapper;
            $('.full-height-panel').each(function(){
                $(this).css('min-height', freeViewPortHeight);
            });

            $("a.lazy").Lazy({
                effectTime: 500,
                effect : "fadeIn"
            });
        })(jQuery); // end of jQuery name space
    </script>
@stop