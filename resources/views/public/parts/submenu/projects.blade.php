<div class="no-print hide-on-med-and-down" id="submenu">
    <nav class="grey darken-3 text-white">
        <div class="container">
            <ul class="uppercase">
                <div class="row center-align uppercase">
                    <li class="col s2 offset-l8"><a href="#about">{{trans('menu.projects.submenu.sens')}}</a></li>
                    <li class="col s2"><a href="#team">{{trans('menu.projects.submenu.tendy')}}</a></li>
                </div>
            </ul>
        </div>
    </nav>
</div>