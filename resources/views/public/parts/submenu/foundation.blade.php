<div class="no-print navbar-fixed hide-on-med-and-down" id="submenu">
    <nav class="grey darken-3 text-white">
        <div class="container">
            <ul class=" uppercase">
            <div class="row uppercase center-align">
                <li class="col s12 l2"><a href="#about">{{trans('menu.foundation.submenu.about')}}</a></li>
                <li class="col s12 l2"><a href="#">{{trans('menu.foundation.submenu.mission')}}</a></li>
                <li class="col s12 l2"><a href="#team">{{trans('menu.foundation.submenu.team')}}</a></li>
                <li class="col s12 l2"><a href="#">{{trans('menu.foundation.submenu.policy')}}</a></li>
                <li class="col s12 l2"><a href="#">{{trans('menu.foundation.submenu.support')}}</a></li>
                <li class="col s12 l2"><a href="#">{{trans('menu.foundation.submenu.contact')}}</a></li>
            </div>
            </ul>
        </div>
    </nav>
</div>