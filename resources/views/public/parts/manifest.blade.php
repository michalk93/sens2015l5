<div class="white-text" style="padding-top: 50px">
    <div class="row">
        <div class="col s12 l10 offset-l1" style="text-align: justify" id="manifest-text">
            <h5 class="center-align">S.E.N.S <br/> Super Energia Na S.E.N.S</h5>
            <br/><br/>
            <p>Celem wydarzenia S.E.N.S jest ingerencja kultury i&nbsp;sztuki w przestrzeń miasta.</p>
            <p>S.E.N.S ma stanowić zdecydowany i&nbsp;wyrazisty przykład prezentacji wielkogabarytowej sztuki w&nbsp;przestrzeni miasta, o&nbsp;co inicjator wydarzenia, Maciej Jurkowski, zabiega od wielu lat.</p>
            <p class="center-align" style="padding: 40px 0px">„Małe jest piękne, ale wielkie obezwładnia”*</p>
            <p>Na świecie, wielkogabarytowa sztuka znakomicie funkcjonuje i&nbsp;fascynuje, zachęca do poszukiwania sensu w przestrzeni miejskiej.</p>
            <p>Tworzymy dzieło: intrygujące, fascynujące, doskonałe artystycznie, perfekcyjnie zaprojektowane i&nbsp;zrealizowane. Sztuka wykorzystująca innowacyjne technologie pozwala wszystkim poruszającym się w przestrzeni miejskiej odnaleźć świat marzeń, wyobraźni, kształtuje i&nbsp;rozwija doznania artystyczne i&nbsp;zmysłowe.</p>
            <p>W Polsce przestrzeń publiczna nie istnieje. Dzięki temu wydarzeniu ma zyskać S.E.N.S.</p>
            <p>S.E.N.S to innowacyjne działanie kulturotwórcze, interaktywne połączenie techniki i&nbsp;sztuki.</p>
            <p>S.E.N.S w naszym zamiarze, wzorem podobnych wydarzeń organizowanych na świecie, ma pełnić rolę czasowego kulturalnego centrum Miasta.</p>
            <p>Do realizacji przedsięwzięcia zapraszamy partnerów, artystów, którzy we współpracy autorskiej stworzą doskonałe: wystawy, koncerty, spektakle teatralne, baletowe, projekcje filmowe, instalacje i&nbsp;inne wydarzenia artystyczne z&nbsp;zastosowaniem najnowszych osiągnięć multimedialnych.</p>
            <p>W projekcie biorą udział profesjonaliści, ale zapraszamy również młodych twórców.</p>
            <p>Udział może wziąć każdy, pod jednym warunkiem - musi być najlepszy!</p>
            <p>Strona internetowa www.sens2015.pl pełni rolę forum wymiany informacji, komunikacji i&nbsp;koordynacji działań pomiędzy partnerami, procesu tworzenia wydarzenia. Wraz z&nbsp;zakończeniem etapu budowy, stanie się ona forum informacyjnym o&nbsp;wydarzeniach planowanych w&nbsp;ramach realizacji projektu.</p>
            <p>Czasowa egzystencja S.E.N.S w Mieście, to symbol istnienia i przemijania, to wyrażenie idei, które inspirują, intrygują i poruszają.</p>
            <p>Rekurencja S.E.N.S to symbol planowanego odradzania się wydarzenia i&nbsp;doskonalenia działań artystycznych.</p>
            <p>S.E.N.S w&nbsp;trakcie swojej działalności może zaskakiwać nieprzewidywalnymi efektami.</p>
            <p>Wierzymy, że dzięki realizacji SENS powstanie przestrzeń miejska.</p>
            <p>Przestrzeń miejska całej Polski potrzebuje zaistnienia w niej SZTUKI.</p>
            <br/>
            <br/>
            <div class="right-align" style="margin-right: 100px; font-size: 1.3em">Zespół S.E.N.S</div>
            <br/>
            <br/>
            <p ><sup>*cytat - Grzegorz Korytowski</sup></p>
        </div>
    </div>
</div>