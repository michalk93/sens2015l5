<div class="navbar-fixed no-print" id="top-nav-wrapper">
    <nav class="grey darken-3 text-white" role="navigation">
        <div class="container">
            <div class="nav-wrapper">
                <a class="brand-logo" href="{{route('home')}}#home">S.E.N.S 2015</a>
                <ul class="right hide-on-med-and-down uppercase" id="menu-main">
                    <li class="{{active_class(if_route(['foundation']))}}"><a href="{{route('foundation')}}">{{trans('menu.foundation.title')}}</a></li>
                    {{--<li>--}}
                        {{--<a href="{{route('home')}}#sens">{{trans('menu.foundation.title')}}</a></li>--}}
                    <li class="{{active_class(if_route(['partners']))}}"><a href="{{route('partners')}}">{{trans('menu.partners.title')}}</a></li>

                    <li class="{{active_class(if_route(['projects']))}}"><a  href="{{route('projects')}}">{{trans('menu.projects.title')}}</a></li>
                    <li class="{{active_class(if_route(['newsroom']))}}"><a href="{{route('newsroom')}}">{{trans('menu.newsroom.title')}}</a></li>
                    {{--<li><a href="{{action('PagesController@getJurkowski')}}">Maciej Jurkowski</a></li>--}}
                </ul>

                <ul id="nav-mobile" class="side-nav uppercase">
                    {{--@if(isset($submenu))--}}
                        {{--@include('public.parts.submenu.'.$submenu)--}}
                    {{--@endif--}}
                </ul>
                <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            </div>
        </div>
    </nav>
</div>
@if(isset($submenu))
@include('public.parts.submenu.'.$submenu)
@endif

@section('js')
    @parent
<script type="text/javascript">
$("#nav-mobile").prepend($('#menu-main').html());
$('#submenu').fadeIn('slow');
</script>
@stop