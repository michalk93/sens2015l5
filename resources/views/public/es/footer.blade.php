<footer class="page-footer no-print">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">S.E.N.S</h5>
            </div>
            {{--<div class="col l4 offset-l2 s12">--}}
                {{--<h5 class="white-text">Links</h5>--}}
                {{--<ul>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>--}}
                    {{--<li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Copyright © {{link_to_route('home','SENS 2015')}}
        </div>
    </div>
</footer>