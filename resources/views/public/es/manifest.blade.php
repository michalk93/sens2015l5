<div class="white-text" style="padding-top: 50px">
    <div class="row">
        <div class="col s12 l10 offset-l1" style="text-align: justify" id="manifest-text">
            <h5 class="center-align">S.E.N.S <br/> Super Energía para S.E.N.S.</h5>
            <br/><br/>
            <p>El evento SENS pretende que la cultura y el arte interfieran en el espacio de la ciudad.</p>
            <p>El evento SENS quiere ser una precisa y bien marcada muestra de obras de arte de grandes dimensiones  en el espacio urbano, cosa por la cual Maciej Jurkowski -el promotor del evento, lucha desde hace varios años.</p>
            <p class="center-align" style="padding: 40px 0px">“Lo pequeño es bello pero lo inmenso abruma”*</p>
            <p>En el mundo el arte de grandes dimensiones funciona perfectamente y fascina, estimula para buscar el sentido en el espacio urbano.</p>
            <p>Creamos una obra: intrigante, fascinante, artísticamente excelente, diseñada y realizada perfectamente. El arte que aprovecha las tecnologías innovadoras permite a todas las personas sumergidas en el espacio urbano descubrir el mundo de los sueños y de la imaginación, forma y despliega las impresiones  artísticas y sensuales.</p>
            <p>En Polonia el espacio público no existe. Gracias al SENS ha de recobrar el SENTIDO.</p>
            <p>SENS es un acto cultural innovador, la unión interactiva de la técnica y el arte.</p>
            <p>Es nuestro propósito que SENS,  igual que otros eventos similares organizados en el mundo, desempeñe el papel del  temporal centro cultural de la Ciudad.</p>
            <p>Invitamos a  participar en  esta aventura  a socios, artistas, quienes en mutua cooperación, crearán excelentes: exposiciones, conciertos, espectáculos teatrales, de ballet, presentaciones de películas, instalaciones y otros eventos artísticos, recurriendo a las modernas tecnologías multimedia. </p>
            <p>En el proyecto participan los profesionales, pero invitamos también a los artistas jóvenes.</p>
            <p>Participar puede cada persona, bajo la única condición –¡tiene que ser la mejor!</p>
            <p>La página web www.sens2015.pl es la plataforma de intercambio de informaciones entre los socios, de comunicación, coordinación de las acciones, y del proceso de creación del evento. Al terminar la etapa de construcción se transformará en el foro de intercambio de informaciones  sobre los eventos planeados durante el proyecto.</p>
            <p>La presencia temporal del evento SESN en la Ciudad es el símbolo de la existencia y la transitoriedad, es la expresión de ideas que inspiran, intrigan y conmueven a las personas que colaboran en su creación</p>
            <p>La recursión del  S.E.N.S es el símbolo de una planeada resucitación del evento y del un perfeccionamiento de actos artísticos.</p>
            <p>En curso de sus actividades SENS puede sorprender con efectos inesperados.</p>
            <p>¡Creemos que gracias a este evento podrá renacer  la SENSación del espacio urbano!</p>
            <p>El espacio urbano en  Polonia necesita el ARTE.</p>
            <br/>
            <br/>
            <div class="right-align" style="margin-right: 100px; font-size: 1.3em">Grupo S.E.N.S</div>
            <br/>
            <br/>
            <p ><sup>*cytat - Grzegorz Korytowski</sup></p>
        </div>
    </div>
</div>