@extends('public.master')

@section('title')
    Galeria - Sens 2015
@stop

@section('content')
    <div class="container" id="gallery">
        <div class="row">
            <div class="col s12">
                <div class="grid">
                    <div class="grid-sizer col s12 m4 l3"></div>
                    @foreach($images as $image)
                        <div class="grid-item col s12 m4 l3">
                            <img src="{{asset('assets/images/gallery/'.basename($image[0]))}}" data-w="{{$image[1]}}" data-h="{{$image[2]}}">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    @parent
    {!! HTML::script('assets/js/masonry.pkgd.min.js') !!}
    {!! HTML::script('assets/js/imagesloaded.pkgd.min.js') !!}
    <script>
        var $grid = $('.grid').imagesLoaded( function() {
            $grid.masonry({
                itemSelector: '.grid-item',
                percentPosition: false,
                columnWidth: '.grid-sizer',

            });
        });
    </script>
@stop