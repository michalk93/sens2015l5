<?php
return [
    'foundation' => [
        'title' => 'Fundacja',
        'submenu' => [
            'about' => 'O nas',
            'team' => 'Zespół',
            'mission' => 'Misja',
            'policy' => 'Statut',
            'support' => 'Wsparcie',
            'contact' => 'Kontakt'
        ]
    ],

    'partners' => [
        'title' => 'Partnerzy'
    ],

    'projects' => [
        'title' => 'Projekty',
        'submenu' => [
            'sens' => 'Sens',
            'tendy' => 'Tendy'
        ]
    ],

    'newsroom' => [
        'title' => 'Newsroom'
    ]
];