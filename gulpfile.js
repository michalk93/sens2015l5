var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('public/main.scss', 'public/assets/css/public/main.css');

    //var bowerPackages = [
    //    {from: "bower_modules/Materialize/dist/css/materialize.min.css", to: "public/assets/css/materialize.min.css"},
    //    {from: "bower_modules/Materialize/dist/js/materialize.min.js", to: "public/assets/js/materialize.min.js"},
    //    {from: "bower_modules/fullpage.js/dist/jquery.fullpage.min.css", to: "public/assets/css/jquery.fullpage.min.css"},
    //    {from: "bower_modules/fullpage.js/dist/jquery.fullpage.min.js", to: "public/assets/js/jquery.fullpage.min.js"},
    //];
    //
    //for(var i=0; i < bowerPackages.length; i++){
    //    mix.copy(bowerPackages[i].from, bowerPackages[i].to);
    //}

});
